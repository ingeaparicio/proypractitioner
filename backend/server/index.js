'use strict'
const express = require('express');
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

const Product = require('./models/product')

const app = express();
const port = process.env.PORT || 3200


app.use(bodyParser.urlencoded({extended: false}))

app.use(bodyParser.json());



app.get('/api/product',(req,res)=>{
Product.find({}, (err,products)=>{
    if (err) return res.status(500).send({message: `Request Error: ${err}`})
    if (!products) return res.status(404).send({message: `Product not Found`})
    res.status(200).send({ products })
})
//res.send(200,{products:[]})
//res.status(200).send({products:[]})
})


app.get('/api/product/:productId',(req,res)=>{
let productId = req.params.productId
Product.findById(productId, (err,product)=>{
    if (err) return res.status(500).send({message: `Request Error: ${err}`})
    if (!product) return res.status(404).send({message: `Product not Found`})
    res.status(200).send({ product })
})
    })


    app.post('/api/product',(req,res)=>{
 console.log('POST /api/product')
 console.log(req.body)
 let product = new Product()
 product.nombre=req.body.nombre
 product.edad=req.body.edad
 product.fechaNacimiento=req.body.fechaNacimiento
 product.direccion=req.body.direccion

 product.save((err,productStored)=>{
     if(err) res.status(500).send({message: `Error al guardar en BD. ${err}`})
     
     res.status(200).send({product: productStored})
 })
 })
     

app.put('/api/product/:productId',(req,res)=>{
    let productId=req.params.productId
    let update=req.body
    Product.findByIdAndUpdate(productId,update,(err,productUpdated)=>{
        if(err) res.status(500).send({message: `Error al actualizar el producto. ${err}`})

        
            res.status(200).send({product:productUpdated})

      

    })



 })

 app.delete('/api/product/:productId',(req,res)=>{
    let productId=req.params.productId
    Product.findById(productId,(err,product)=>{
        if(err) res.status(500).send({message: `Error al borrar el producto. ${err}`})

        product.remove(err =>{
            if (err)  res.status(500).send({message: `Error al borrar producto: ${err}`})
            res.status(200).send({message: `El producto se ha eliminado `})

        })

    })


 

})

const URI = 'mongodb://localhost/mean-crud';

mongoose.connect(URI)
    .then(db => console.log('db is connected '))
    .catch(err => console.error(err));

app.listen(port, ()=>{
    console.log(`API REST corriendo en 3200 `)
})