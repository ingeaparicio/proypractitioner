const express = require('express');
const router = express.Router();

const bonoj = require('../controllers/bonoj.controller');

router.get('/', bonoj.getBonosj);
router.post('/', bonoj.createBonoj);
router.get('/:id', bonoj.getBonoj);
router.put('/:id', bonoj.editBonoj);
router.delete('/:id', bonoj.deleteBonoj);

module.exports = router;
