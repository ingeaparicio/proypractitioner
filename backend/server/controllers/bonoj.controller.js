const Bonoj = require('../models/bonoj');

const bonojCtrl = {};

bonojCtrl.getBonosj = async (req, res, next) => {
    const bonosj = await Bonoj.find();
    res.json(bonosj);
};

bonojCtrl.createBonoj = async (req, res, next) => {
    const bonoj = new Bonoj({
    //    name: req.body.name,
    //    position: req.body.position,
    //    office: req.body.office,
    //    salary: req.body.salary


    fechaEmision:req.body.fechaEmision,
    monedaEmision:req.body.monedaEmision,
    subyacente:req.body.subyacente,
   cliente:req.body.cliente,
   crOficina:req.body.crOficina,
   clavePizarra:req.body.clavePizarra




 







    });
    await bonoj.save();
    res.json({status: 'Bonoj created'});
};

bonojCtrl.getBonoj = async (req, res, next) => {
    const { id } = req.params;
    const bonoj = await Bonoj.findById(id);
    res.json(bonoj);
};

bonojCtrl.editBonoj = async (req, res, next) => {
    const { id } = req.params;
    const bonoj = {
     //   name: req.body.name,
       // position: req.body.position,
      //  office: req.body.office,
       // salary: req.body.salary,
       fechaEmision:req.body.fechaEmision,
       monedaEmision:req.body.monedaEmision,
       subyacente:req.body.subyacente,
      cliente:req.body.cliente,
      crOficina:req.body.crOficina,
      clavePizarra:req.body.clavePizarra,
    };
    await Bonoj.findByIdAndUpdate(id, {$set: bonoj}, {new: true});
    res.json({status: 'Bonoj Updated'});
};

bonojCtrl.deleteBonoj = async (req, res, next) => {
    await Bonoj.findByIdAndRemove(req.params.id);
    res.json({status: 'Bonoj Deleted'});
};

module.exports = bonojCtrl;