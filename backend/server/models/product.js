'use strict'

const mongoose = require('mongoose');
const  Schema  = mongoose.Schema;

const ProductSchema =  Schema({
    nombre :String,
    edad: String,
    fechaNacimiento: String,
    direccion:String

})

module.exports=mongoose.model('Product', ProductSchema)
 