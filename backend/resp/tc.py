import http.client
import json


conn = http.client.HTTPSConnection("www.banxico.org.mx")

headers = {
    'cache-control': "no-cache",
    'postman-token': "48072463-df7e-e85e-dc6e-ae3f05685769"
    }

conn.request("GET", "/SieAPIRest/service/v1/series/SF43718/datos/oportuno?token=3d0d1ec73680aecd65890d854363283dd84af46399654b801d06c1d41516e39b", headers=headers)
res = conn.getresponse()
data = res.read()


dataParse = json.loads(data)
print(dataParse["bmx"]["series"][0]["datos"][0]['dato'])
